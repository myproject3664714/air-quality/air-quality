#include <SoftwareSerial.h>
#include <Wire.h>;
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd( 0x27, 16, 2);
#define SIM_TX_PIN 6
#define SIM_RX_PIN 7
SoftwareSerial sim(SIM_TX_PIN, SIM_RX_PIN);
int DHT11_pin = 12;        // DHT11 Digital Pin
int PM25_Pin = A1;        // PM2.5 Analog Pin
int PM25_led_Pin = 11;    // PM2.5 led Pin
int MQ7_Pin = A3;         // MQ7 Analog Pin
int sstled = 8;          // Led Statuts
//int MQ135_Pin = A2;

char value1[16];          // = 'a' = Tem
char value2[16];          // = 'b' = Hum
char value3[16];          // = 'c' = CO
char value4[16];          // = 'd' = PM2.5

bool start;
int i, numId, lengthmang;
String mangdata = "";

void toSerial() {
  while (sim.available() != 0) {
    Serial.write(sim.read());
  }
}

void sim_disConnect_gprs() {
  sim.write("AT+HTTPTERM\r\n");
  delay(2000);
  toSerial();
  sim.write("AT+CGATT=0\r\n");
  delay(3000);
  toSerial();
}
void sim_connect_gprs() {
  sim.write("AT+CGATT=1\r\n");    
  delay(7000);
  toSerial();
  sim.write("AT+SAPBR=1,1\r\n");   
  delay(7000);
  toSerial();
  sim.write("AT+HTTPINIT\r\n"); 
  delay(2000);
  toSerial();
  sim.write("AT+HTTPPARA=\"CID\",1\r\n");
  delay(2000);
  toSerial();
  }

void sim_read_data() {

  sim.write("AT+HTTPPARA=URL,http://qualitiesair.000webhostapp.com/b.txt"); 
  delay(50);
  toSerial();

  sim.write("\r\n");
  delay(2000);
  toSerial();

  digitalWrite(sstled, 1);
  sim.write("AT+HTTPACTION=0\r\n"); 
  delay(10000);
  toSerial();

  sim.write("AT+HTTPREAD\r\n");
  delay(10000);
  mangdata.remove(0);
  while (sim.available() != 0) {     //tính hiệu khác 0
    mangdata += (char)sim.read();  //công dồn mảng
  }
  digitalWrite(sstled, 0);
  Serial.println(mangdata);
  lengthmang = mangdata.length();

  for (i = 0; i < lengthmang; i++) {
    if ((mangdata[i] == 79) && (mangdata[i + 1] == 78)) {
      start = true;
    }
    if ((mangdata[i] == 79) && (mangdata[i + 1] == 70) && (mangdata[i + 2] == 70)) {
      start = false;
    }
  }

}

void sim_send_data(char * value1, char * value2, char * value3, char * value4)
{
  int counterror = 0;

a:
  sim.write("AT+HTTPPARA=URL,http://qualitiesair.000webhostapp.com/?a=");
  delay(50);
  toSerial();

  sim.write(value1);
  delay(50);
  toSerial();

  sim.write("&b=");
  delay(50);
  toSerial();

  sim.write(value2);
  delay(50);
  toSerial();

  sim.write("&c=");
  delay(50);
  toSerial();

  sim.write(value3);
  delay(50);
  toSerial();

  sim.write("&d=");
  delay(50);
  toSerial();

  sim.write(value4);
  delay(50);
  toSerial();

  sim.write("\r\n");
  delay(50);
  delay(2000);
  toSerial();
  digitalWrite(sstled, 1);
  sim.write("AT+HTTPACTION=0\r\n"); 
  delay(13000);
  mangdata.remove(0);
  while (sim.available() != 0) {
    mangdata += (char)sim.read();
  }
  digitalWrite(sstled, 0);
  Serial.println(mangdata);
  lengthmang = mangdata.length();
  for (i = 0; i < lengthmang; i++)
  {
    if (((mangdata[i] == 44) && (mangdata[i + 1] == 54) && (mangdata[i + 2] == 48) && (mangdata[i + 4] == 44)) || ((mangdata[i] == 69) && (mangdata[i + 1] == 82) && (mangdata[i + 2] == 82))) { //,60, hoặc ERR
      Serial.println("Data send error, resend");
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Sent Error");  
      lcd.setCursor(0, 1);
      lcd.print("Resent...");
      counterror++;
      if (counterror == 3) {
        counterror = 0;
        sim_disConnect_gprs();
        sim_connect_gprs();
      }
      goto a;
    }
  }

}
// ĐỌC CẢM BIẾN NHIỆT ĐỘ - ĐỘ ẨM
void DHT11() {

  uint16_t rawHumidity = 0;
  uint16_t rawTemperature = 0;
  uint8_t checkSum = 0;
  uint16_t data = 0;
  uint8_t Hm;
  uint8_t Tem;
  unsigned long startTime;

  pinMode(DHT11_pin, OUTPUT);
  digitalWrite(DHT11_pin, LOW);
  delay(18);
  digitalWrite(DHT11_pin, HIGH);
  pinMode(DHT11_pin, INPUT);
  digitalWrite(DHT11_pin, HIGH);

  for ( int8_t i = 0 ; i < 80; i++ )
  {
    again1:
    byte live;
    startTime = micros();

    do
    {
      live = (unsigned long)(micros() - startTime);
      if ( live > 90 ) {
        return;
      }
    }
    while ( digitalRead(DHT11_pin) == (i & 1) ? HIGH : LOW );

    if ( i >= 0 && (i & 1) )
    {
      data <<= 1;// TON of bit 0 is maximum 30 usecs and of bit 1 is at least 68 usecs.
      if ( live > 30 )
      {
        data |= 1;
      }
    }

    switch ( i ) {
      case 31:
        rawHumidity = data;
        break;
      case 63:
        rawTemperature = data;
      case 79:
        checkSum = data;
        data = 0;
        break;
    }
      
    if((byte)checkSum == (byte)(Hm + Tem)){
    }
    else{ goto again1;
    }
    Hm = rawHumidity >> 8;
    Tem = rawTemperature >> 8;
    }
  
  dtostrf(Tem, 0, 1, value1);
  dtostrf(Hm, 0, 0, value2);
}

// ĐỌC CẢM BIẾN KHÍ CO, 1000ms = 1s
void MQ7() {
  float gtcotucthoi = 0, analogMQ7 = 0, co;
  double tongco = 0;
  for (i = 0; i < 50; i++) {
    analogMQ7 = analogRead(MQ7_Pin);                      
    gtcotucthoi = analogMQ7 * 990.0 / 1023.0 + 10.0;
    tongco = gtcotucthoi + tongco;
    delay(20);
  }
  co = tongco / 50.0;
  dtostrf(co, 0, 0, value3);
}

// ĐỌC CẢM BIẾN BỤI, 1050ms = 1.05s
void PM25() {
  float gtbuitucthoi = 0, b = 0, tongb = 0, dienap=0;
  for (i = 0; i < 30; i++) {             // Thực hiện đọc 20 lần,
    int analogPM25 = 0;
   
    digitalWrite(PM25_led_Pin, 0);        // Bật IR LED
    delayMicroseconds(280);               // Delay 0.28ms
    analogPM25 = analogRead(PM25_Pin);
    delayMicroseconds(40);                // Delay 0.04ms
    digitalWrite(PM25_led_Pin, 1);        // Tắt LED
    delayMicroseconds(9680);              // Delay 9.28ms
    gtbuitucthoi = (float)analogPM25 / 1023.0;
//    dienap = (float)analogPM25 * (5.0 / 1023.0); //    //Điệp áp Vcc của cảm biến
//    gtbuitucthoi = 0.17 * dienap - 0.1;// Linear equation
    tongb = tongb + gtbuitucthoi;
    delay(25);
  }
  b = tongb / 30.0;
  dtostrf(b, 0, 3, value4);
}

void setup() {
  pinMode(sstled, OUTPUT);
  pinMode(PM25_led_Pin, OUTPUT);
  Serial.begin(9600);
  sim.begin(9600);
  delay(1000);
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Loading...");
  delay(9000);
  sim_disConnect_gprs();
  sim_connect_gprs();
  sim_read_data();
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Ok...");
}
void loop() {
  if (start == true) {
    DHT11();
    MQ7();
    PM25();

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Tem:");
    lcd.print(value1);
    lcd.setCursor(10, 0);
    lcd.print("CO:");
    lcd.print(value3);
    lcd.setCursor(0, 1);
    lcd.print("Hm:");
    lcd.print(value2);
    lcd.setCursor(6, 1);
    lcd.print("PM25:");
    lcd.print(value4);
    delay(250);

    sim_send_data(value1, value2, value3, value4);
    delay(250);
    sim_read_data();
    delay(250);
  }
  if (start == false) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Sleeping...");
    sim_read_data();
    delay(250);
  }
}
