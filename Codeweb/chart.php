<!DOCTYPE html>
<html>
<head>
    <!-- <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, intial-scale=1">
    <link rel="stylesheet" href="style.css"> -->
    <meta charset="utf8">
	<meta name="viewport" content="width=device-width, intial-scale=1"/> 
	<link rel="stylesheet" href="style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script> 

<?php 
// $username = "u874965802_mysvtest"; // Khai báo username kết nối đén database

// $password = "Binhthuong@1";      // Khai báo passwor
// $server   = "localhost";   // Khai báo server
// $dbname   = "u874965802_mysvtest";      // Khai báo database
$username = "id20693237_phongth1"; // Khai báo username
$password = "Pz]5fGAupy6D^m}b";      // Khai báo password
$server   = "localhost";   // Khai báo server
$dbname   = "id20693237_airquality";      // Khai báo database
if($_GET){
    $conn = new mysqli($server, $username, $password, $dbname); //mở kết nối
    if(isset($_GET["keyday"])){
        $ngaythang = $_GET['keyday']; //kiểm tra biến
        $sql = "SELECT * FROM bang_data WHERE day='$ngaythang'"; //trỏ đén bang_data trong database
        $title = $ngaythang;
    } else{
        $sql = "SELECT * FROM bang_data"; //trỏ đén bang_data trong database
        $title = "Tất cả các ngày";
    }
	if ($result = $conn->query($sql)) {  //truy vấn cơ sở dữ liệu
		while ($row = $result->fetch_assoc()) {    //nạp kêt quả dưới dạng mảng kết họp
			$row_tem = $row["tem"];
			$row_hum = $row["hum"];
			$row_co = $row["co"];
			$row_pm25 = $row["pm25"];
			$time1= strtotime($row['time'])*1000;
			$data1[] = "[$time1,$row_tem]";
			$data2[] = "[$time1,$row_hum]";
			$data3[] = "[$time1,$row_co]";
			$data4[] = "[$time1,$row_pm25]";
		}
		$result->free();
	}   
}
?>
<script type="text/javascript"> //viets bagnw jvs
$(function () { Highcharts.setOptions({ //cài đặt tùy chọn của biểu đồ
    lang: {
        months: [
            'mot', 'hai', 'ba', 'bon',
            'nam', 'sau', 'bay', 'tam',
            'chin', 'muoi', 'muoi mot', 'muoi hai'],
        weekdays: [
            'Chủ nhật', 'Thứ hai', 'Thứ ba', 'Thứ tư',
            'Thứ năm', 'Thứ sáu', 'Thứ bảy'],
        shortMonths: [
            'T1', 'T2', 'T3', 'T4',
            'T5', 'T6', 'T7', 'T8',
            'T9', 'T10', 'T11', 'T12']
    }
	});
Highcharts.chart('container1', {
	title: {text: 'Nhiệt độ'},
	subtitle: {text: 'Sensor DHT11'},
	yAxis: {
		title: {text: 'độ C'}
	},
	xAxis: {type: 'datetime',},
	legend: {
		layout: 'vertical',
		align: 'right',
		verticalAlign: 'middle'
	},
	plotOptions: {
        line: {
            dataLabels: {enabled: true},
            enableMouseTracking: true
        }
    },
	series: [{
		name: 'Tem',
		data: [<?php echo join(',', $data1) ?>],
		type: 'area',
		marker: { enabled: false},
		lineColor: Highcharts.getOptions().colors[8],
		color: Highcharts.getOptions().colors[8],
		fillOpacity: 0.2,
	}],
    responsive: {
        rules: [{
            condition: { maxWidth: 500},
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    },
    credits: {enabled: false}
});

Highcharts.chart('container2', {
	title: { text: 'Độ ẩm không khí'},
	subtitle: { text: 'Sensor DHT11'},
	yAxis: {
		title: {text: '%'}
	},
	xAxis: {type: 'datetime',},
	legend: {
		layout: 'vertical',
		align: 'right',
		verticalAlign: 'middle'
	},
	plotOptions: {
        line: {
            dataLabels: { enabled: true},
            enableMouseTracking: true
        }
    },
	series: [{
		name: 'Hum',
		data: [<?php echo join(',', $data2); ?>],
		type: 'area',
		lineColor: Highcharts.getOptions().colors[0],
		color: Highcharts.getOptions().colors[9],
		fillOpacity: 0.2,
		marker: { enabled: false},
	}],
    responsive: {
        rules: [{
            condition: {maxWidth: 500 },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    },
    credits: {enabled: false}
});

Highcharts.chart('container3', {
	title: { text: 'Nồng độ khí CO'},
	subtitle: { text: 'Sensor MQ-7'},
	yAxis: {
		title: {text: 'ppm'}
	},
	xAxis: {type: 'datetime',},
	legend: {
		layout: 'vertical',
		align: 'right',
		verticalAlign: 'middle'
	},
	plotOptions: {
        line: {
            dataLabels: {enabled: true},
            enableMouseTracking: true
        }
    },
	series: [{
		name: 'CO',
		data: [<?php echo join(',', $data3); ?>],
		type: 'area',
		lineColor: Highcharts.getOptions().colors[3],
		color: Highcharts.getOptions().colors[3],
		fillOpacity: 0.2,
		marker: {enabled: false},
	}],
    responsive: {
        rules: [{
            condition: {maxWidth: 500},
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    },
    credits: {enabled: false}
});

Highcharts.chart('container4', {
	title: {text: 'Nồng độ bụi mịn 2.5M'},
	subtitle: {text: 'Sensor SHARP GP2Y10'},
	yAxis: {
		title: {text: 'mg/m3'}
	},
	xAxis: {type: 'datetime',},
	legend: {
		layout: 'vertical',
		align: 'right',
		verticalAlign: 'middle'
	},
	plotOptions: {
        line: {
            dataLabels: {enabled: true},
            enableMouseTracking: true
        },
    },
	series: [{
		name: '2.5PM',
		data: [<?php echo join(',', $data4); ?>],
		type: 'area',
		lineColor: Highcharts.getOptions().colors[1],
		color: Highcharts.getOptions().colors[1],
		fillOpacity: 0.2,
		marker: {enabled: false},
	}],
    responsive: {
        rules: [{
            condition: {maxWidth: 500},
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    },
    credits: {enabled: false}
});
});
</script>
</head>
<body>
<div class="highcharts-figure">
  <div id="container1" class="chart"></div>
  <div id="container2" class="chart"></div>
  <div id="container3" class="chart"></div>
  <div id="container4" class="chart"></div>
</div>
</body>
</html>	